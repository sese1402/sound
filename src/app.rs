use std::{
    io::{self, Stdout},
    time::Duration,
};

use crossterm::event::{self, Event, KeyCode};
use ratatui::prelude::*;
use ratatui::Terminal;

use crate::components::liste::DeviceList;

pub type Tui = Terminal<CrosstermBackend<Stdout>>;

#[derive(PartialEq, Eq)]
pub enum Fenster {
    Input,
    Output,
}

impl Default for Fenster {
    fn default() -> Self {
        Fenster::Input
    }
}

#[derive(Default)]
pub struct App {
    exit: bool,
    fenster: Fenster,
    output: DeviceList,
    input: DeviceList,
}

impl App {
    pub fn run(&mut self, terminal: &mut Tui) -> io::Result<()> {
        while !self.exit {
            self.output.update_output();
            self.input.update_input();
            terminal.draw(|frame| self.render_frame(frame))?;
            self.handle_events()?;
        }
        Ok(())
    }

    fn render_frame(&mut self, frame: &mut Frame) {
        let layout =
            Layout::horizontal(vec![Constraint::Percentage(50), Constraint::Percentage(50)])
                .flex(layout::Flex::Center)
                .split(frame.size());
        if self.fenster == Fenster::Output {
            self.output
                .build(frame, layout[1], "output devices", Color::Blue);
            self.input
                .build(frame, layout[0], "input devices", Color::White);
        }
        if self.fenster == Fenster::Input {
            self.output
                .build(frame, layout[1], "output devices", Color::White);
            self.input
                .build(frame, layout[0], "input devices", Color::Blue);
        }
    }

    fn handle_events(&mut self) -> io::Result<()> {
        if event::poll(Duration::from_millis(250))? {
            if let Event::Key(key) = event::read()? {
                if KeyCode::Char('q') == key.code {
                    self.exit = true;
                }
                if KeyCode::Char('h') == key.code {
                    self.fenster = Fenster::Input;
                }
                if KeyCode::Char('l') == key.code {
                    self.fenster = Fenster::Output;
                }
                if KeyCode::Char('j') == key.code {
                    self.fenster = Fenster::Input;
                }
                if KeyCode::Char('k') == key.code {
                    self.fenster = Fenster::Output;
                }

                // if KeyCode::Char('H') == key.code {
                //     self.fenster = Fenster::Input;
                // }
                // if KeyCode::Char('L') == key.code {
                //     self.fenster = Fenster::Output;
                // }
                // if KeyCode::Char('J') == key.code {
                //     self.fenster = Fenster::Input;
                // }
                // if KeyCode::Char('K') == key.code {
                //     self.fenster = Fenster::Output;
                // }
            }
        }

        Ok(())
    }
}
