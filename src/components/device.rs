use pulsectl::controllers::types::DeviceInfo;
use ratatui::{prelude::*, widgets::*};

#[derive(Default, Debug, Clone)]
pub struct Device {
    name: String,
    mute: bool,
    volume: u32,
}

impl Into<Device> for DeviceInfo {
    fn into(self) -> Device {
        Device {
            name: self.description.unwrap(),
            mute: self.mute,
            volume: self.volume.avg().0,
        }
    }
}

impl Device {
    pub fn build(&self, frame: &mut Frame, pos: Rect) {
        let border = Block::bordered().border_type(BorderType::Rounded);

        let inner_layout_pos = border.inner(pos);

        let layout_horizontal = Layout::vertical(vec![
            Constraint::Percentage(33),
            Constraint::Percentage(33),
            Constraint::Percentage(33),
        ])
        .split(inner_layout_pos);

        let layout_row_one =
            Layout::horizontal(vec![Constraint::Percentage(50), Constraint::Percentage(50)])
                .split(layout_horizontal[0]);
        let layout_row_three =
            Layout::horizontal(vec![Constraint::Percentage(100)]).split(layout_horizontal[2]);

        let empulse = LineGauge::default()
            .label(format! {
                "{:.0}%",
                (self.volume as f64 / u16::MAX  as f64) * 100f64,
            })
            .filled_style(
                Style::default()
                    .fg(Color::Blue)
                    .bg(Color::Black)
                    .add_modifier(Modifier::BOLD),
            )
            .line_set(symbols::line::THICK)
            .ratio((self.volume as f64 / u16::MAX as f64 / 1.5).clamp(0., 1.));
        let name_para = Paragraph::new(Line::from(self.name.clone()).left_aligned());

        let mute = match self.mute {
            true => "unmute",
            false => "mute",
        };

        let mute_para = Paragraph::new(Line::from(mute).right_aligned());

        frame.render_widget(empulse, layout_row_three[0]);
        frame.render_widget(name_para, layout_row_one[0]);
        frame.render_widget(mute_para, layout_row_one[1]);
        frame.render_widget(border, pos);
    }
}
