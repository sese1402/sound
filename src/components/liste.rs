use crate::Device;

use pulsectl::controllers::{DeviceControl, SinkController, SourceController};
use ratatui::{prelude::*, widgets::*};

#[derive(Default, Debug, Clone)]
pub struct DeviceList {
    liste: Vec<Device>,
}

impl DeviceList {
    pub fn build(&mut self, frame: &mut Frame, pos: Rect, title: &str, color: Color) {
        let output_block = Block::bordered()
            .border_type(BorderType::Rounded)
            .border_style(Style::new().fg(color))
            .title_top(title);

        let mut rect_for_device = output_block.inner(pos);

        for d in self.liste.clone() {
            rect_for_device.height = 7;
            d.build(frame, rect_for_device);
            rect_for_device.y += 7;
        }

        frame.render_widget(output_block, pos);
    }

    pub fn update_output(&mut self) {
        let mut controller = SinkController::create().unwrap();

        self.liste = controller
            .list_devices()
            .unwrap()
            .into_iter()
            .map(|d| d.into())
            .collect::<Vec<Device>>();
    }

    pub fn update_input(&mut self) {
        let mut controller = SourceController::create().unwrap();

        self.liste = controller
            .list_devices()
            .unwrap()
            .into_iter()
            .map(|d| d.into())
            .collect::<Vec<Device>>();
    }
}
